// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include "Collatz.h"

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

using namespace std;

// Cache that stores cycle length of numbers (0 < num < 1000000)
// calculated during calls to collatz_eval()
short lazy_cache[1000000] = {};

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    assert(i > 0);
    assert(i < 1000000);
    assert(j > 0);
    assert(j < 1000000);
    // Iterate from the smaller number to the larger number
    int begin = i < j ? i : j;
    int end = i < j ? j : i;
    assert(begin <= end);
    // Optimized to reduce the amount of numbers to iterate through
    // Given positive integers, b and e, let m = (e / 2) + 1
    // If b < m, then max_cycle_length(b, e) = max_cycle_length(m, e)
    int mid = (end / 2) + 1;
    if(begin < mid) {
        begin = mid;
    }
    assert(begin <= end);

    short cycle_length;
    short max_cycle_length = 0;
    long temp_num;

    // Compare cycle lengths of numbers in interval
    for(int num = begin; num < end + 1; ++num) {
        temp_num = num;
        // Get cached value if found and skip calculations
        if(lazy_cache[temp_num] != 0) {
            if(lazy_cache[temp_num] > max_cycle_length) {
                max_cycle_length = lazy_cache[temp_num];
            }
        } else {
            cycle_length = 1;
            while(temp_num > 1) {
                if(temp_num % 2 == 1) {
                    // Optimized to calculate 2 steps at a time on odd numbers
                    temp_num += (temp_num >> 1) + 1;
                    // Append cached value if found
                    if(temp_num < 1000000 && lazy_cache[temp_num] != 0) {
                        cycle_length += lazy_cache[temp_num] + 1;
                        break;
                    }
                    cycle_length += 2;
                } else {
                    // Even numbers
                    temp_num /= 2;
                    // Append cached value if found
                    if(temp_num < 1000000 && lazy_cache[temp_num] != 0) {
                        cycle_length += lazy_cache[temp_num];
                        break;
                    }
                    ++cycle_length;
                }
            }
            // Add newly calculated cycle length to cache
            lazy_cache[num] = cycle_length;
            if(cycle_length > max_cycle_length) {
                max_cycle_length = cycle_length;
            }
        }
    }
    assert(max_cycle_length > 0);
    return max_cycle_length;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
